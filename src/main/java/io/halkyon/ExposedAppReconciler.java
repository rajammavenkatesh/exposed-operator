package io.halkyon;

import java.util.Map;

import io.fabric8.kubernetes.api.model.ObjectMeta;
import io.fabric8.kubernetes.api.model.ObjectMetaBuilder;
import io.fabric8.kubernetes.api.model.apps.DeploymentBuilder;
import io.fabric8.kubernetes.client.KubernetesClient;
import io.javaoperatorsdk.operator.api.reconciler.Context;
import io.javaoperatorsdk.operator.api.reconciler.Reconciler;
import io.javaoperatorsdk.operator.api.reconciler.UpdateControl;

public class ExposedAppReconciler implements Reconciler<ExposedApp> { 
private final KubernetesClient client;

  public ExposedAppReconciler(KubernetesClient client) {
    this.client = client;
  }

  private ObjectMeta createMetadata(ExposedApp resource, Map<String, String> labels){
    final var metadata=resource.getMetadata();
    return new ObjectMetaBuilder()
        .withName(metadata.getName())
        .addNewOwnerReference()
            .withUid(metadata.getUid())
            .withApiVersion(resource.getApiVersion())
            .withName(metadata.getName())
            .withKind(resource.getKind())
        .endOwnerReference()
        .withLabels(labels)
    .build();
}

  @Override
  public UpdateControl<ExposedApp> reconcile(ExposedApp exposedApp, Context context) {
    final var name=exposedApp.getMetadata().getName();
    final var spec=exposedApp.getSpec();
    final var imageRef=spec.getImageRef();
    final var labels=spec.getLabels();

    var deployment =new DeploymentBuilder()
        .withMetadata(createMetadata(exposedApp, labels))
        .withNewSpec()
            .withNewSelector().withMatchLabels(labels).endSelector()
            .withNewTemplate()
                .withNewMetadata().withLabels(labels).endMetadata()
                .withNewSpec()
                    .addNewContainer()
                        .withName(name).withImage(imageRef)
                        .addNewPort()
                            .withName("http").withProtocol("TCP").withContainerPort(8080)
                        .endPort()
                    .endContainer()
                .endSpec()
            .endTemplate()
        .endSpec()
    .build();

client.apps().deployments().createOrReplace(deployment);

    return UpdateControl.noUpdate();
  }
}
