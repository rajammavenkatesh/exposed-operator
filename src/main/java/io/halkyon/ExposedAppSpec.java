package io.halkyon;

import java.util.Map;

public class ExposedAppSpec {

    private Map<String, String> labels;

    private String imageRef;

    public Map<String, String> getLabels() { return labels; }

    public String getImageRef() {
        return imageRef;
    }

}
